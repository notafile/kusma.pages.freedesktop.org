---
title: Getting Started
permalink: /download/
redirect_from: /download.html
---

## Downloading

Primary Mesa download site:
[ftp.freedesktop.org](ftp://ftp.freedesktop.org/pub/mesa/) (FTP) or
[mesa.freedesktop.org](https://mesa.freedesktop.org/archive/) (HTTPS).

Starting with the first release of 2017, Mesa's version scheme is
year-based. Filenames are in the form `mesa-Y.N.P.tar.gz`, where `Y` is
the year (two digits), `N` is an incremental number (starting at 0) and
`P` is the patch number (0 for the first release, 1 for the first patch
after that).

When a new release is coming, release candidates (betas) may be found in
the same directory, and are recognisable by the `mesa-Y.N.P-rcX.tar.gz`
filename.

## Unpacking

Mesa releases are available in two formats: `.tar.xz` and `.tar.gz`.

To unpack the tarball:

<kbd>tar xf mesa-Y.N.P.tar.xz</kbd>

or

<kbd>tar xf mesa-Y.N.P.tar.gz</kbd>

## Contents

Proceed to the [compilation and installation
instructions]({{ site.docs_url }}/install.html).

## Demos, GLUT, and GLU

A package of SGI's GLU library is available
[here](ftp://ftp.freedesktop.org/pub/mesa/glu/)

A package of Mark Kilgard's GLUT library is available
[here](ftp://ftp.freedesktop.org/pub/mesa/glut/)

The Mesa demos collection is available
[here](ftp://ftp.freedesktop.org/pub/mesa/demos/)

In the past, GLUT, GLU and the Mesa demos were released in conjunction
with Mesa releases. But since GLUT, GLU and the demos change
infrequently, they were split off into their own git repositories:
[GLUT](https://gitlab.freedesktop.org/mesa/glut),
[GLU](https://gitlab.freedesktop.org/mesa/glu) and
[Demos](https://gitlab.freedesktop.org/mesa/demos),
