---
title: News
permalink: /news/
path: pages/news.md
pagination:
  enabled: true
  per_page: 5
  trail:
    before: 2
    after: 2
  permalink: ':num/'
  sort_field: 'date'
  sort_reverse: true
---
{% for post in paginator.posts %}
<h2>{{ post.title }}</h2>

{{ post.excerpt }}

<a href="{{ post.url }}">Read full post</a>
<hr>
{% endfor %}

{% if paginator.total_pages > 1 %}
<nav aria-label="Page navigation">
  <ul class="pagination">
  {% if paginator.previous_page %}
    <li class="page-item">
      <a class="page-link" href="{{ paginator.first_page_path }}">&laquo;</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="{{ paginator.previous_page_path }}">&lsaquo;</a>
    </li>
  {% else %}
    <li class="page-item disabled">
      <span class="page-link">&laquo;</span>
    </li>
    <li class="page-item disabled">
      <span class="page-link">&lsaquo;</span>
    </li>
  {% endif %}

  {% if paginator.page_trail %}
    {% for trail in paginator.page_trail %}
    <li class="page-item{% if page.url == trail.path %} active{% endif %}">
      <a class="page-link" href="{{ trail.path | prepend: site.baseurl }}" title="{{trail.title}}">{{ trail.num }}</a>
    </li>
    {% endfor %}
  {% endif %}

  {% if paginator.next_page %}
    <li class="page-item">
      <a class="page-link" href="{{ paginator.next_page_path }}">&rsaquo;</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="{{ paginator.last_page_path }}">&raquo;</a>
    </li>
  {% else %}
    <li class="page-item disabled">
      <span class="page-link">&rsaquo;</span>
    </li>
    <li class="page-item disabled">
      <span class="page-link">&raquo;</span>
    </li>
  {% endif %}
  </ul>
</nav>
{% endif %}
