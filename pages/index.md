---
title: Home
layout: home
permalink: /
---

<style>
  [data-toggle="collapse"] > svg.octicon-chevron-up {
    display: inline-block;
    float: right;
    width: 1em;
    height: 1em;
    transition: all 0.2s;
  }
  [data-toggle="collapse"].collapsed > svg.octicon-chevron-up {
    transform: rotate(180deg);
  }
</style>

<h2>
  <a class="collapsed" role="button" data-toggle="collapse" data-target="#apis-collapse" aria-expanded="false" aria-controls="apis-collapse">
    Featured APIs
    {% octicon chevron-up %}
  </a>
</h2>

<div class="row pt-3 collapse" id="apis-collapse">
  {% for api in site.data.apis %}
  <div class="col-12 col-md-6 col-xl-4 pb-3">
    {% if api.logo %}
    <img class="img-fluid" style="height: 3.5rem" src="{{ site.baseurl }}/assets/apis/{{ api.logo | uri_escape }}" alt="{{ api.name }}">
    {% else %}
    <h3>{{ api.name }}</h3>
    {% endif %}
    <p>{{ api.body }}</p>
    <p><a class="btn btn-secondary" href="{{ api.url }}" role="button">Read more »</a></p>
  </div>
  {% endfor %}
</div>

---

<h2>
  <a class="collapsed" role="button" data-toggle="collapse" data-target="#drivers-collapse" aria-expanded="false" aria-controls="drivers-collapse">
    Supported Drivers
    {% octicon chevron-up %}
  </a>
</h2>

<dl class="row pt-3 collapse" id="drivers-collapse">
  {% for driver in site.drivers %}
  <dt class="col-md-3 col-lg-2">{{ driver.name }}</dt>
  <dd class="col-md-9 col-lg-10">
    <p>{{ driver.content }}
      {% if driver.external_url %}
      <a href="{{ driver.external_url }}">More information&hellip;</a>
      {% elsif driver.docs_article %}
      <a href="{{ site.docs_url }}/{{ driver.docs_article }}">More information&hellip;</a>
      {% endif %}
    </p>
  </dd>
  {% endfor %}
</dl>

---

## Latest news

{% for post in site.posts limit:3 %}

### {{ post.title }}

{{ post.excerpt }}

[Read full post]({{ post.url }})

---

{% endfor %}

[Read all news]({{ site.baseurl }}/news/)
