---
title:    "John Carmack made an interesting .plan update yesterday"
date:     1999-05-01 00:00:00
category: misc
tags:     []
---
John Carmack made an interesting .plan update yesterday:

    I put together a document on optimizing OpenGL drivers for Q3 that should be helpful to the various Linux 3D teams.
    
    http://www.quake3arena.com/news/glopt.html
