---
title:    "Mesa's directory tree has been overhauled"
date:     2003-06-08 00:00:00
category: misc
tags:     []
---
Mesa's directory tree has been overhauled. Things are better organized
now with some thought toward future needs.

In CVS, the latest Mesa 5.1 development code is now rooted under the
**Mesa-newtree/** directory. The old top-level **Mesa/** directory holds
the Mesa 5.0.x code which will be abandoned at some point.
