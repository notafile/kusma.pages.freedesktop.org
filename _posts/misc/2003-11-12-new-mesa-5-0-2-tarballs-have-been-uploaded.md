---
layout:   post
title:    "New Mesa 5.0.2 tarballs have been uploaded"
date:     2003-11-12 00:00:00
category: misc
tags:     []
---
New Mesa 5.0.2 tarballs have been uploaded to SourceForge which fix a
number of automake/libtool problems.

The new MD5 checksums are:

    a9dcf3ff9ad1b7d6ce73a0df7cff8b5b  MesaLib-5.0.2.tar.gz
    7b4bf9261657c2fca03796d4955e6f50  MesaLib-5.0.2.tar.bz2
    79c141bddcbad557647535d02194f346  MesaLib-5.0.2.zip
    952d9dc823dd818981d1a648d7b2668a  MesaDemos-5.0.2.tar.gz
    b81fafff90995025d2f25ea02b786642  MesaDemos-5.0.2.tar.bz2
    a21be975589e8a2d1871b6bb7874fffa  MesaDemos-5.0.2.zip
