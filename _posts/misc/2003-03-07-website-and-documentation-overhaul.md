---
title:    "Website and documentation overhaul"
date:     2003-03-07 00:00:00
category: misc
tags:     []
---
Website and documentation overhaul.

The website content and Mesa documentation (from the doc/ directory)
have been merged together. All the documentation files have been entered
into the CVS repository. Many of the old plain-text files have been
converted to html and modernized.
