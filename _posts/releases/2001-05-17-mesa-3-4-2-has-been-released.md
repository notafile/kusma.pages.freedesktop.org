---
title:    "Mesa 3.4.2 has been released"
date:     2001-05-17 00:00:00
category: releases
tags:     []
---
Mesa 3.4.2 has been released. This is basically just a bug-fix release.
Here's what's new:

``` 
    Bug fixes:
        - deleting the currently bound texture could cause bad problems
        - using fog could result in random vertex alpha values
         - AA triangle rendering could touch pixels outside right window bound
        - fixed byteswapping problem in clear_32bit_ximage() function
        - fixed bugs in wglUseFontBitmapsA(), by Frank Warmerdam
        - fixed memory leak in glXUseXFont()
        - fragment sampling in AA triangle function was off by 1/2 pixel
        - Windows: reading pixels from framebuffer didn't always work
        - glConvolutionFilter2D could segfault or cause FP exception
        - fixed segfaults in FX and X drivers when using tex unit 1 but not 0
        - GL_NAND logicop didn't work right in RGBA mode
        - fixed a memory corruption bug in vertex buffer reset code
        - clearing the softwara alpha buffer with scissoring was broken
        - fixed a few color index mode fog bugs
        - fixed some bad assertions in color index mode
        - fixed FX line 'stipple' bug #420091
    Changes:
        - optimized writing mono-colored pixel spans to X pixmaps
        - increased max viewport size to 2048 x 2048
```
