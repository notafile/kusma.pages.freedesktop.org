---
title:    "Mesa 17.2.7 is released"
date:     2017-12-14 00:00:00
category: releases
tags:     []
---
[Mesa 17.2.7](relnotes/17.2.7.html) is released. This is a bug-fix
release.
