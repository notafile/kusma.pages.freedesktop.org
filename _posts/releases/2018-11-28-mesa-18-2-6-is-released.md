---
title:    "Mesa 18.2.6 is released"
date:     2018-11-28 00:00:00
category: releases
tags:     []
---
[Mesa 18.2.6](relnotes/18.2.6.html) is released. This is a bug-fix
release.
