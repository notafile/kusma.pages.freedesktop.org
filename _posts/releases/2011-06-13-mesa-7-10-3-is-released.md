---
title:    "Mesa 7.10.3 is released"
date:     2011-06-13 00:00:00
category: releases
tags:     []
---
[Mesa 7.10.3](relnotes/7.10.3.html) is released. This is a bug fix
release.
