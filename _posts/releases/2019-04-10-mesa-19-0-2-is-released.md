---
title:    "Mesa 19.0.2 is released"
date:     2019-04-10 00:00:00
category: releases
tags:     []
---
[Mesa 19.0.2](relnotes/19.0.2.html) is released. This is a bug-fix
release.
