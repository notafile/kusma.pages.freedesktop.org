---
title:    "Mesa 19.0.5 is released"
date:     2019-05-21 00:00:00
category: releases
tags:     []
---
[Mesa 19.0.5](relnotes/19.0.5.html) is released. This is a bug-fix
release.
