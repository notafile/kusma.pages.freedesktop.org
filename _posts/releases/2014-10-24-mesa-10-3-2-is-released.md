---
title:    "Mesa 10.3.2 is released"
date:     2014-10-24 00:00:00
category: releases
tags:     []
---
[Mesa 10.3.2](relnotes/10.3.2.html) is released. This is a bug-fix
release.
