---
title:    "Mesa 17.0.7 is released"
date:     2017-06-01 00:00:00
category: releases
tags:     []
---
[Mesa 17.0.7](relnotes/17.0.7.html) is released. This is a bug-fix
release.

NOTE: It is anticipated that 17.0.7 will be the final release in the
17.0 series. Users of 17.0 are encouraged to migrate to the 17.1 series
in order to obtain future fixes.
