---
title:    "Mesa 18.0.2 is released"
date:     2018-04-28 00:00:00
category: releases
tags:     []
---
[Mesa 18.0.2](relnotes/18.0.2.html) is released. This is a bug-fix
release.
