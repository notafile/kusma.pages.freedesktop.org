---
title:    "Mesa 11.1.1 is released"
date:     2016-01-13 00:00:00
category: releases
tags:     []
---
[Mesa 11.1.1](relnotes/11.1.1.html) is released. This is a bug-fix
release.
