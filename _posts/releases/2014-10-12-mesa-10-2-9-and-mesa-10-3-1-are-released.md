---
title:    "Mesa 10.2.9 and Mesa 10.3.1 are released"
date:     2014-10-12 00:00:00
category: releases
tags:     []
---
[Mesa 10.2.9](relnotes/10.2.9.html) and [Mesa
10.3.1](relnotes/10.3.1.html) are released. These are bug-fix releases
from the 10.2 and 10.3 branches, respectively.

NOTE: It is anticipated that 10.2.9 will be the final release in the
10.2 series. Users of 10.2 are encouraged to migrate to the 10.3 series
in order to obtain future fixes.
