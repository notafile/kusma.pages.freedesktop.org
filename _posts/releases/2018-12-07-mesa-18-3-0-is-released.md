---
title:    "Mesa 18.3.0 is released"
date:     2018-12-07 00:00:00
category: releases
tags:     []
---
[Mesa 18.3.0](relnotes/18.3.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
