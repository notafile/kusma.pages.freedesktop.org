---
title:    "Mesa 6.5 has been released"
date:     2006-03-31 00:00:00
category: releases
tags:     []
---
[Mesa 6.5](relnotes/6.5.html) has been released. This is a new
development release.
