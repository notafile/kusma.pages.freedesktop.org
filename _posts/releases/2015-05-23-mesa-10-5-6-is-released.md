---
title:    "Mesa 10.5.6 is released"
date:     2015-05-23 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.6](relnotes/10.5.6.html) is released. This is a bug-fix
release.
