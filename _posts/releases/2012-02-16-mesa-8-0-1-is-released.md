---
title:    "Mesa 8.0.1 is released"
date:     2012-02-16 00:00:00
category: releases
tags:     []
---
[Mesa 8.0.1](relnotes/8.0.1.html) is released. This is a bug fix
release. See the release notes for more information about the release.
