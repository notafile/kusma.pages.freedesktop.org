---
title:    "Mesa 17.0.0 is released"
date:     2017-02-13 00:00:00
category: releases
tags:     []
---
[Mesa 17.0.0](relnotes/17.0.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
