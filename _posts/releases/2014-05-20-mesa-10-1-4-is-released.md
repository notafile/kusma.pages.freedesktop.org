---
title:    "Mesa 10.1.4 is released"
date:     2014-05-20 00:00:00
category: releases
tags:     []
---
[Mesa 10.1.4](relnotes/10.1.4.html) is released. This is a bug-fix
release.
