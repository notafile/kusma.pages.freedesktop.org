---
title:    "Mesa 7.4.3 is released"
date:     2009-06-19 00:00:00
category: releases
tags:     []
---
[Mesa 7.4.3](relnotes/7.4.3.html) is released. This is a stable release
fixing bugs since the 7.4.2 release.
