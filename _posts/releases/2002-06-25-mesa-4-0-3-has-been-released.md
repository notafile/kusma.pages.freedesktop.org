---
title:    "Mesa 4.0.3 has been released"
date:     2002-06-25 00:00:00
category: releases
tags:     []
---
Mesa 4.0.3 has been released. This is a stable bug-fix release.

``` 
    New:
    - updated GL/glext.h file (version 15)
    - corrected MMX blend code (Jose Fonseca)
    - support for software-based alpha planes in Windows driver
    - updated GGI driver (Filip Spacek)
    Bug fixes:
    - glext.h had wrong values for GL_DOT3_RGB[A]_EXT tokens
    - OSMesaMakeCurrent() didn't recognize buffer size changes
    - assorted conformance fixes for 16-bit/channel rendering
    - texcombine alpha subtraction mode was broken
    - fixed lighting bug with non-uniform scaling and display lists
    - fixed bug when deleting shared display lists
    - disabled SPARC cliptest assembly code (Mesa bug 544665)
    - fixed a couple Solaris compilation/link problems
    - blending clipped glDrawPixels didn't always work
    - glGetTexImage() didn't accept packed pixel types
    - glPixelMapu[is]v() could explode given too large of pixelmap
    - glGetTexParameter[if]v() didn't accept GL_TEXTURE_MAX_ANISOTROPY_EXT
    - glXCopyContext() could lead to segfaults
    - glCullFace(GL_FRONT_AND_BACK) didn't work (bug 572665)
    Changes:
    - lots of C++ (g++) code clean-ups
    - lots of T&L updates for the Radeon DRI driver
    Known bugs:
    - mipmap LOD computation (fixed for Mesa 4.1)
```
