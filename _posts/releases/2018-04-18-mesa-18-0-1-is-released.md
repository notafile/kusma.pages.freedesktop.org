---
title:    "Mesa 18.0.1 is released"
date:     2018-04-18 00:00:00
category: releases
tags:     []
---
[Mesa 18.0.1](relnotes/18.0.1.html) is released. This is a bug-fix
release.
