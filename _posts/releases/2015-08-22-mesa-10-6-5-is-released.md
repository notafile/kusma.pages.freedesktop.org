---
title:    "Mesa 10.6.5 is released"
date:     2015-08-22 00:00:00
category: releases
tags:     []
---
[Mesa 10.6.5](relnotes/10.6.5.html) is released. This is a bug-fix
release.
