---
title:    "Mesa 19.2.1 is released"
date:     2019-10-09 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.1](relnotes/19.2.1.html) is released. This is a bug fix
release.
