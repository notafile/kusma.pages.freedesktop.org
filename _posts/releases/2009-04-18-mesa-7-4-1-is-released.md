---
title:    "Mesa 7.4.1 is released"
date:     2009-04-18 00:00:00
category: releases
tags:     []
---
[Mesa 7.4.1](relnotes/7.4.1.html) is released. This is a stable release
fixing bugs since the 7.4 release.
