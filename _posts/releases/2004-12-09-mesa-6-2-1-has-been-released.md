---
title:    "Mesa 6.2.1 has been released"
date:     2004-12-09 00:00:00
category: releases
tags:     []
---
Mesa 6.2.1 has been released. This is a stable release which just fixes
bugs since the 6.2 release.

``` 
    Bug fixes:
    - don't apply regular fog or color sum when using a fragment program
    - glProgramEnvParameter4fARB always generated an error on
      GL_FRAGMENT_PROGRAM_ARB (fdo bug 1645)
    - glVertexAttrib3svNV and glVertexAttrib3svARB were broken
    - fixed width/height mix-up in glSeparableFilter2D()
    - fixed regression in glCopyPixels + convolution
    - glReadPixels from a clipped front color buffer didn't always work
    - glTexImage didn't accept GL_RED/GREEN/BLUE as the format
    - Attempting queries/accesses of VBO 0 weren't detected as errors
    - paletted textures failed if the palette had fewer than 256 entries
    Changes:
    - fixed a bunch of compiler warnings found with gcc 3.4
    - bug reports should to go bugzilla.freedesktop.org
```

The MD5 checksums are:

    80008a92f6e055d3bfdde2cf331ec3fa  MesaLib-6.2.1.tar.gz
    f43228cd2bf70f583ef3275c1c545421  MesaLib-6.2.1.tar.bz2
    dec26cfd40116ad021020fea2d94f652  MesaLib-6.2.1.zip
    2c7af3c986a7571c8713c8bfee7e49e3  MesaDemos-6.2.1.tar.gz
    3cac74667b50bcbd4f67f594fb4224a2  MesaDemos-6.2.1.tar.bz2
    75b3edd12eb2b370caf05f29b99e508a  MesaDemos-6.2.1.zip
