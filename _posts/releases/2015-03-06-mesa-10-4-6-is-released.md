---
title:    "Mesa 10.4.6 is released"
date:     2015-03-06 00:00:00
category: releases
tags:     []
---
[Mesa 10.4.6](relnotes/10.4.6.html) is released. This is a bug-fix
release.
