---
title:    "Mesa 10.1.3 is released"
date:     2014-05-09 00:00:00
category: releases
tags:     []
---
[Mesa 10.1.3](relnotes/10.1.3.html) is released. This is a bug-fix
release, and is being released sooner than originally scheduled to fix a
performance regression (vmware swapbuffers falling back to software)
introduced to the 10.1.2 release.
