---
title:    "Mesa 19.1.6 is released"
date:     2019-09-03 00:00:00
category: releases
tags:     []
---
[Mesa 19.1.6](relnotes/19.1.6.html) is released. This is a bug-fix
release.
