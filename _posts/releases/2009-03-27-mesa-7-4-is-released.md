---
title:    "Mesa 7.4 is released"
date:     2009-03-27 00:00:00
category: releases
tags:     []
---
[Mesa 7.4](relnotes/7.4.html) is released. This is a stable release
fixing bugs since the 7.3 release.
