---
title:    "Mesa 18.3.1 is released"
date:     2018-12-11 00:00:00
category: releases
tags:     []
---
[Mesa 18.3.1](relnotes/18.3.1.html) is released. This is a bug-fix
release.
