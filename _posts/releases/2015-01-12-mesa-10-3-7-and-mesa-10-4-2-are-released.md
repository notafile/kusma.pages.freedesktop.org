---
title:    "Mesa 10.3.7 and Mesa 10.4.2 are released"
date:     2015-01-12 00:00:00
category: releases
tags:     []
---
[Mesa 10.3.7](relnotes/10.3.7.html) and [Mesa
10.4.2](relnotes/10.4.2.html) are released. These are bug-fix releases
from the 10.3 and 10.4 branches, respectively.

NOTE: It is anticipated that 10.3.7 will be the final release in the
10.3 series. Users of 10.3 are encouraged to migrate to the 10.4 series
in order to obtain future fixes.
