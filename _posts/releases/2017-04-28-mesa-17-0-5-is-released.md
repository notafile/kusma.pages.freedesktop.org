---
title:    "Mesa 17.0.5 is released"
date:     2017-04-28 00:00:00
category: releases
tags:     []
---
[Mesa 17.0.5](relnotes/17.0.5.html) is released. This is a bug-fix
release.
