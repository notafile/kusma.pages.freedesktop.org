---
title:    "Mesa 7.8.2 is released"
date:     2010-06-16 00:00:00
category: releases
tags:     []
---
[Mesa 7.8.2](relnotes/7.8.2.html) is released. This is a bug-fix release
collecting fixes since the 7.8.1 release.
