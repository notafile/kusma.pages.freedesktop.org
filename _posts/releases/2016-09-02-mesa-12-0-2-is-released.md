---
title:    "Mesa 12.0.2 is released"
date:     2016-09-02 00:00:00
category: releases
tags:     []
---
[Mesa 12.0.2](relnotes/12.0.2.html) is released. This is a bug-fix
release.
