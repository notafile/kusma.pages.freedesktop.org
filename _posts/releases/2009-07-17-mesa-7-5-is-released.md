---
title:    "Mesa 7.5 is released"
date:     2009-07-17 00:00:00
category: releases
tags:     []
---
[Mesa 7.5](relnotes/7.5.html) is released. This is a new features
release. People especially concerned about stability may want to wait
for the follow-on 7.5.1 bug-fix release.
