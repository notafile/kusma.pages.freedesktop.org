---
title:    "Mesa 13.0.4 is released"
date:     2017-02-01 00:00:00
category: releases
tags:     []
---
[Mesa 13.0.4](relnotes/13.0.4.html) is released. This is a bug-fix
release.
