---
title:    "Mesa 13.0.6 and Mesa 17.0.2 are released"
date:     2017-03-20 00:00:00
category: releases
tags:     []
---
[Mesa 13.0.6](relnotes/13.0.6.html) and [Mesa
17.0.2](relnotes/17.0.2.html) are released. These are bug-fix releases
from the 13.0 and 17.0 branches, respectively.

NOTE: It is anticipated that 13.0.6 will be the final release in the
13.0 series. Users of 13.0 are encouraged to migrate to the 17.0 series
in order to obtain future fixes.
