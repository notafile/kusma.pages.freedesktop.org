---
title:    "Mesa 6.2 has been released"
date:     2004-10-02 00:00:00
category: releases
tags:     []
---
Mesa 6.2 has been released. This is a stable release which just fixes
bugs since the 6.1 release.

``` 
    New:
    - enabled GL_ARB_texture_rectangle (same as GL_NV_texture_rectangle)
    - updated Doxygen support (Jose Fonseca)
    Changes:
    - some GGI driver updates (Christoph Egger, bug 1025977)
    Bug fixes:
    - Omit GL_ARB_texture_non_power_of_two from list of OpenGL 1.5 features
    - fixed a few compilation issues on IRIX
    - fixed a matrix classification bug (reported by Wes Bethel)
    - we weren't reseting the vertex/fragment program error state
      before parsing (Dave Reveman)
    - adjust texcoords for sampling texture rectangles (Dave Reveman)
    - glGet*(GL_MAX_VERTEX_ATTRIBS_ARB) wasn't implemented
    - repeated calls to glDeleteTexture(t) could lead to a crash
    - fixed potential ref count bugs in VBOs and vertex/fragment programs
    - spriteblast demo didn't handle window size changes correctly
    - glTexSubImage didn't handle pixels=NULL correctly for PBOs
    - fixed color index mode glDrawPixels bug (Karl Schultz)
```

The MD5 checksums are:

    9e8f34b059272dbb8e1f2c968b33bbf0  MesaLib-6.2.tar.gz
    3d6a6362390b6a37d3cb2e615f3ac7db  MesaLib-6.2.tar.bz2
    6cfd7895d28e695c0dbbed9469564091  MesaLib-6.2.zip
    3e06e33b0809f09855cb60883b8bdfef  MesaDemos-6.2.tar.gz
    9d160009c3dfdb35fe7e4088c9ba8f85  MesaDemos-6.2.tar.bz2
    856f7ec947122eb3c8985ebc2f654dcd  MesaDemos-6.2.zip
