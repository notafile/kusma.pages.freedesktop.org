---
title:    "Mesa 18.1.6 is released"
date:     2018-08-13 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.6](relnotes/18.1.6.html) is released. This is a bug-fix
release.
