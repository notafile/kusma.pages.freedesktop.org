---
title:    "Mesa 10.3 is released"
date:     2014-09-19 00:00:00
category: releases
tags:     []
---
[Mesa 10.3](relnotes/10.3.html) is released. This is a new development
release. See the release notes for more information about the release.

Also, [Mesa 10.2.8](relnotes/10.2.8.html) is released. This is a bug fix
release from the 10.2 branch.
