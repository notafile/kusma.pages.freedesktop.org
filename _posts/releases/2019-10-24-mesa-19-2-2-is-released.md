---
title:    "Mesa 19.2.2 is released"
date:     2019-10-24 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.2](relnotes/19.2.2.html) is released. This is a bug fix
release.
