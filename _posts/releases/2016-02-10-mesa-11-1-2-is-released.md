---
title:    "Mesa 11.1.2 is released"
date:     2016-02-10 00:00:00
category: releases
tags:     []
---
[Mesa 11.1.2](relnotes/11.1.2.html) is released. This is a bug-fix
release.
