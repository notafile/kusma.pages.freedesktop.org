---
title:    "Mesa 6.4.2 has been released"
date:     2006-02-02 00:00:00
category: releases
tags:     []
---
[Mesa 6.4.2](relnotes/6.4.2.html) has been released. This is stable,
bug-fix release.
