---
title:    "Mesa 19.1.7 is released"
date:     2019-09-17 00:00:00
category: releases
tags:     []
---
[Mesa 19.1.7](relnotes/19.1.7.html) is released. This is a bug-fix
release.
