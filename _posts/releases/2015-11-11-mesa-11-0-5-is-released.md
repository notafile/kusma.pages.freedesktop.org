---
title:    "Mesa 11.0.5 is released"
date:     2015-11-11 00:00:00
category: releases
tags:     []
---
[Mesa 11.0.5](relnotes/11.0.5.html) is released. This is a bug-fix
release.
