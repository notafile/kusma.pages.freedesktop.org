---
title:    "Mesa 10.5.1 is released"
date:     2015-03-13 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.1](relnotes/10.5.1.html) is released. This is a bug-fix
release.
