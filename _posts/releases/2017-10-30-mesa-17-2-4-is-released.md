---
title:    "Mesa 17.2.4 is released"
date:     2017-10-30 00:00:00
category: releases
tags:     []
---
[Mesa 17.2.4](relnotes/17.2.4.html) is released. This is a bug-fix
release.
