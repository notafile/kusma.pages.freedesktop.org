---
title:    "Mesa 3.4.1 has been released"
date:     2001-02-14 00:00:00
category: releases
tags:     []
---
Mesa 3.4.1 has been released. Here's what's new:

``` 
    New:
        - fixed some Linux build problems
        - fixed some Windows build problems
        - GL_EXT_texture_env_dot3 extension (Gareth Hughes)
    Bug fixes:
        - added RENDER_START/RENDER_FINISH macros for glCopyTexImage in DRI
        - various state-update code changes needed for DRI bugs
        - disabled pixel transfer ops in glColorTable commands, not needed
        - fixed bugs in glCopyConvolutionFilter1D/2D, glGetConvolutionFilter
        - updated sources and fixed compile problems in widgets-mesa/
        - GLX_PBUFFER enum value was wrong in glx.h
        - fixed a glColorMaterial lighting bug
        - fixed bad args to Read/WriteStencilSpan in h/w stencil clear function
        - glXCopySubBufferMESA() Y position was off by one
        - Error checking of glTexSubImage3D() was broken (bug 128775)
        - glPopAttrib() didn't restore all derived Mesa state correctly
        - Better glReadPixels accuracy for 16bpp color - fixes lots of OpenGL
          conformance problems at 16bpp.
        - clearing depth buffer with scissoring was broken, would segfault
        - OSMesaGetDepthBuffer() returned bad bytesPerValue value
        - fixed a line clipping bug (reported by Craig McDaniel)
        - fixed RGB color over/underflow bug for very tiny triangles
    Known problems:
        - NURBS or evaluator surfaces inside display lists don't always work
```
