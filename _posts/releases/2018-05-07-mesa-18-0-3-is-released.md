---
title:    "Mesa 18.0.3 is released"
date:     2018-05-07 00:00:00
category: releases
tags:     []
---
[Mesa 18.0.3](relnotes/18.0.3.html) is released. This is a bug-fix
release.
