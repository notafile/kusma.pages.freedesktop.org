---
title:    "Mesa 19.2.4 is released"
date:     2019-11-13 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.4](relnotes/19.2.4.html) is released. This is an emergency
bugfix release, all users of 19.2.3 are recomended to upgrade
immediately.
