---
title:    "Mesa 3.2 has been released"
date:     2000-04-24 00:00:00
category: releases
tags:     []
---
Mesa 3.2 has been released. Here's what's new since the beta release:

``` 
    Bug fixes:
    - fixed memcpy bugs in span.c
    - fixed missing glEnd problem in demos/tessdemo.c
    - fixed bug when clearing 24bpp Ximages
    - fixed clipping problem found in Unreal Tournament
    - fixed Loki's "ice bug" and "crazy triangles" seen in Heretic2
    - fixed Loki's 3dfx RGB vs BGR bug
    - fixed Loki's 3dfx smooth/flat shading bug in SoF
    Changes:
    - updated docs/README file
    - use bcopy() optimizations on FreeBSD
    - re-enabled the optimized persp_textured_triangle() function
```
