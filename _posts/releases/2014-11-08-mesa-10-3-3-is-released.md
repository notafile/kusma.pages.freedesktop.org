---
title:    "Mesa 10.3.3 is released"
date:     2014-11-08 00:00:00
category: releases
tags:     []
---
[Mesa 10.3.3](relnotes/10.3.3.html) is released. This is a bug-fix
release.
