---
title:    "Mesa 12.0.3 is released"
date:     2016-09-15 00:00:00
category: releases
tags:     []
---
[Mesa 12.0.3](relnotes/12.0.3.html) is released. This is a bug-fix
release.
