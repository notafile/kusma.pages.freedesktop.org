---
title:    "Mesa 9.2.2 is released"
date:     2013-10-18 00:00:00
category: releases
tags:     []
---
[Mesa 9.2.2](relnotes/9.2.2.html) is released. This is a bug fix
release.
