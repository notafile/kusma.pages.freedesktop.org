---
title:    "Mesa 19.2.6 is released"
date:     2019-11-21 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.6](relnotes/19.2.6.html) is released. This is a bug fix
release.
