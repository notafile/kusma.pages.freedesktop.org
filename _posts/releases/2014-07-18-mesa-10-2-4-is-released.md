---
title:    "Mesa 10.2.4 is released"
date:     2014-07-18 00:00:00
category: releases
tags:     []
---
[Mesa 10.2.4](relnotes/10.2.4.html) is released. This is a bug-fix
release.
