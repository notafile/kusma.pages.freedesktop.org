---
title:    "Mesa 17.2.1 is released"
date:     2017-09-17 00:00:00
category: releases
tags:     []
---
[Mesa 17.2.1](relnotes/17.2.1.html) is released. This is a bug-fix
release.
