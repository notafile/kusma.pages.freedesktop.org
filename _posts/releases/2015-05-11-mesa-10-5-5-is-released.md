---
title:    "Mesa 10.5.5 is released"
date:     2015-05-11 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.5](relnotes/10.5.5.html) is released. This is a bug-fix
release.
