---
title:    "Mesa 13.0.3 is released"
date:     2017-01-05 00:00:00
category: releases
tags:     []
---
[Mesa 13.0.3](relnotes/13.0.3.html) is released. This is a bug-fix
release.
