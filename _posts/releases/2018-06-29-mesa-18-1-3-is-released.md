---
title:    "Mesa 18.1.3 is released"
date:     2018-06-29 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.3](relnotes/18.1.3.html) is released. This is a bug-fix
release.
