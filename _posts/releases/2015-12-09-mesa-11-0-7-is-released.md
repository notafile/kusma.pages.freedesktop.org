---
title:    "Mesa 11.0.7 is released"
date:     2015-12-09 00:00:00
category: releases
tags:     []
---
[Mesa 11.0.7](relnotes/11.0.7.html) is released. This is a bug-fix
release.

Mesa demos 8.3.0 is also released. See the
[announcement](https://lists.freedesktop.org/archives/mesa-announce/2015-December/000191.html)
for more information about the release. You can download it from
[ftp.freedesktop.org/pub/mesa/demos/8.3.0/](ftp://ftp.freedesktop.org/pub/mesa/demos/8.3.0/).
