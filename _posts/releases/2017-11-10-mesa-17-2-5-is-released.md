---
title:    "Mesa 17.2.5 is released"
date:     2017-11-10 00:00:00
category: releases
tags:     []
---
[Mesa 17.2.5](relnotes/17.2.5.html) is released. This is a bug-fix
release.
