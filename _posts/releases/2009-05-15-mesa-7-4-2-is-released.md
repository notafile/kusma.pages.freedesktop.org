---
title:    "Mesa 7.4.2 is released"
date:     2009-05-15 00:00:00
category: releases
tags:     []
---
[Mesa 7.4.2](relnotes/7.4.2.html) is released. This is a stable release
fixing bugs since the 7.4.1 release.
