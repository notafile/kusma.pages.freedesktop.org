---
title:    "Mesa 4.0.4 has been released"
date:     2002-10-03 00:00:00
category: releases
tags:     []
---
Mesa 4.0.4 has been released. This is a stable bug-fix release.

``` 
    New:
    - GL_NV_texture_rectangle extension
    - updated glext.h header (version 17)
    - updated DOS driver (Daniel Borca)
    - updated BeOS R5 driver (Philippe Houdoin)
    - added GL_IBM_texture_mirror_repeat
    - glxinfo now takes -l option to print interesting OpenGL limits info
    - GL_MESA_ycbcr_texture extension
    - GL_APPLE_client_storage extension (for some DRI drivers only)
    - GL_MESA_pack_invert extension
    Bug fixes:
    - fixed GL_LINEAR fog bug by adding clamping
    - fixed FP exceptions found using Alpha CPU
    - 3dfx MESA_GLX_FX=window (render to window) didn't work
    - fixed memory leak in wglCreateContest (Karl Schultz)
    - define GLAPIENTRY and GLAPI if undefined in glu.h
    - wglGetProcAddress didn't handle all API functions
    - when testing for OpenGL 1.2 vs 1.3, check for GL_ARB_texture_cube_map
    - removed GL_MAX_CONVOLUTION_WIDTH/HEIGHT from glGetInteger/Float/etc()
    - error checking in compressed tex image functions had some glitches
    - fixed AIX compile problem in src/config.c
    - glGetTexImage was using pixel unpacking instead of packing params
    - auto-mipmap generation for cube maps was incorrect
    Changes:
    - max texture units reduced to six to accommodate texture rectangles
    - removed unfinished GL_MESA_sprite_point extension code
```
