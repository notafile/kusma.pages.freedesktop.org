---
title:    "Mesa 10.4.3 is released"
date:     2015-01-24 00:00:00
category: releases
tags:     []
---
[Mesa 10.4.3](relnotes/10.4.3.html) is released. This is a bug-fix
release.
