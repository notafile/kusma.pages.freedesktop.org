---
title:    "Mesa 18.1.5 is released"
date:     2018-07-27 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.5](relnotes/18.1.5.html) is released. This is a bug-fix
release.
