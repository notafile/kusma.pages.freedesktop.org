---
title:    "Mesa 19.2.5 is released"
date:     2019-11-20 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.5](relnotes/19.2.5.html) is released. This is a bug fix
release.
