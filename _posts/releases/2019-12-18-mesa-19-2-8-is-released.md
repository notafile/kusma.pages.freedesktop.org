---
title:    "Mesa 19.2.8 is released"
date:     2019-12-18 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.8](relnotes/19.2.8.html) is released. This is a bug fix
release.
