---
title:    "Mesa 10.2.5 is released"
date:     2014-08-02 00:00:00
category: releases
tags:     []
---
[Mesa 10.2.5](relnotes/10.2.5.html) is released. This is a bug-fix
release.
