---
title:    "Mesa 9.1.1 is released"
date:     2013-03-19 00:00:00
category: releases
tags:     []
---
[Mesa 9.1.1](relnotes/9.1.1.html) is released. This is a bug fix
release.
