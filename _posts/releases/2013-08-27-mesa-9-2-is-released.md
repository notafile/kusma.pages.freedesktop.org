---
title:    "Mesa 9.2 is released"
date:     2013-08-27 00:00:00
category: releases
tags:     []
---
[Mesa 9.2](relnotes/9.2.html) is released. This is a new development
release. See the release notes for more information about the release.
