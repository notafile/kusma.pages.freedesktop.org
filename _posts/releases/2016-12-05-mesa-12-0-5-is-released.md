---
title:    "Mesa 12.0.5 is released"
date:     2016-12-05 00:00:00
category: releases
tags:     []
---
[Mesa 12.0.5](relnotes/12.0.5.html) is released. This is a bug-fix
release.

NOTE: It is anticipated that 12.0.5 will be the final release in the
12.0 series. Users of 12.0 are encouraged to migrate to the 13.0 series
in order to obtain future fixes.
