---
title:    "Mesa 10.0.1 and Mesa 9.2.5 are released"
date:     2013-12-12 00:00:00
category: releases
tags:     []
---
[Mesa 10.0.1](relnotes/10.0.1.html) and [Mesa
9.2.5](relnotes/9.2.5.html) are released. These are both bug-fix
releases.
