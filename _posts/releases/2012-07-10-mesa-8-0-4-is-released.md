---
title:    "Mesa 8.0.4 is released"
date:     2012-07-10 00:00:00
category: releases
tags:     []
---
[Mesa 8.0.4](relnotes/8.0.4.html) is released. This is a bug fix
release.
