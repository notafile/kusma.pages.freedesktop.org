---
title:    "Mesa 10.5.9 is released"
date:     2015-07-04 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.9](relnotes/10.5.9.html) is released. This is a bug-fix
release.

NOTE: It is anticipated that 10.5.9 will be the final release in the
10.5 series. Users of 10.5 are encouraged to migrate to the 10.6 series
in order to obtain future fixes.
