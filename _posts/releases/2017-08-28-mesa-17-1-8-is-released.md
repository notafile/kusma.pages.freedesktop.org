---
title:    "Mesa 17.1.8 is released"
date:     2017-08-28 00:00:00
category: releases
tags:     []
---
[Mesa 17.1.8](relnotes/17.1.8.html) is released. This is a bug-fix
release.
