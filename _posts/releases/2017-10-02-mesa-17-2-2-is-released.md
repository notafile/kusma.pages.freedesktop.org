---
title:    "Mesa 17.2.2 is released"
date:     2017-10-02 00:00:00
category: releases
tags:     []
---
[Mesa 17.2.2](relnotes/17.2.2.html) is released. This is a bug-fix
release.
