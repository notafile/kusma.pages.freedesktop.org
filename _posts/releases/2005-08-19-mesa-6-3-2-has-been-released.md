---
title:    "Mesa 6.3.2 has been released"
date:     2005-08-19 00:00:00
category: releases
tags:     []
---
Mesa 6.3.2 has been released. Note: there was no public release of
version 6.3.1.

[Changes in version 6.3.1](versions.html#6.3.2)

The MD5 checksums are:

    98192e45ed8d69113688f89f90869346  MesaLib-6.3.2.tar.gz
    0df27701df0924d17ddf41185efa8ce1  MesaLib-6.3.2.tar.bz2
    ccb2423aab77fc7e81ce628734586140  MesaLib-6.3.2.zip
    9d0fca0a7d051c34a0b485423fb3e85d  MesaDemos-6.3.2.tar.gz
    96708868450c188205e42229b5d813c4  MesaDemos-6.3.2.tar.bz2
    c5102501e609aa8996d832fafacb8ab9  MesaDemos-6.3.2.zip
