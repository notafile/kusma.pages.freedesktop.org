---
title:    "Mesa 10.0.4 is released"
date:     2014-03-12 00:00:00
category: releases
tags:     []
---
[Mesa 10.0.4](relnotes/10.0.4.html) is released. This is a bug-fix
release.
