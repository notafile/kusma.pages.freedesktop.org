---
title:    "Mesa 18.1.8 and Mesa 18.2.0 are released"
date:     2018-09-07 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.8](relnotes/18.1.8.html) and [Mesa
18.2.0](relnotes/18.2.0.html) are released. These are, respectively, a
bug-fix release from the 18.1 branch and a new development release. See
the release notes for more information about the releases.
