---
title:    "Mesa 11.0.3 is released"
date:     2015-10-10 00:00:00
category: releases
tags:     []
---
[Mesa 11.0.3](relnotes/11.0.3.html) is released. This is a bug-fix
release.
