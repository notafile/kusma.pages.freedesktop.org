---
title:    "Mesa 19.2.3 is released"
date:     2019-11-06 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.3](relnotes/19.2.3.html) is released. This is a bug fix
release.
