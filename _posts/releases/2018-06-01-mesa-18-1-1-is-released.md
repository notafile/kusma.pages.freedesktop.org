---
title:    "Mesa 18.1.1 is released"
date:     2018-06-01 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.1](relnotes/18.1.1.html) is released. This is a bug-fix
release.
