---
title:    "Mesa 19.2.0 is released"
date:     2019-09-25 00:00:00
category: releases
tags:     []
---
[Mesa 19.2.0](relnotes/19.2.0.html) is released. This is a new
development release. See the release notes for more information about
this release
