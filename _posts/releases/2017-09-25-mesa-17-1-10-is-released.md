---
title:    "Mesa 17.1.10 is released"
date:     2017-09-25 00:00:00
category: releases
tags:     []
---
[Mesa 17.1.10](relnotes/17.1.10.html) is released. This is a bug-fix
release.

NOTE: It is anticipated that 17.1.10 will be the final release in the
17.1 series. Users of 17.1 are encouraged to migrate to the 17.2 series
in order to obtain future fixes.
