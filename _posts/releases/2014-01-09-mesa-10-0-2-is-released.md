---
title:    "Mesa 10.0.2 is released"
date:     2014-01-09 00:00:00
category: releases
tags:     []
---
[Mesa 10.0.2](relnotes/10.0.2.html) is released. This is a bug-fix
release.
