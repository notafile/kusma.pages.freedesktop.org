---
title:    "Mesa 19.1.3 is released"
date:     2019-07-23 00:00:00
category: releases
tags:     []
---
[Mesa 19.1.3](relnotes/19.1.3.html) is released. This is a bug-fix
release.
