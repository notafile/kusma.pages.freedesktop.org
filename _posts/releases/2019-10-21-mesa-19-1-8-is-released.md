---
title:    "Mesa 19.1.8 is released"
date:     2019-10-21 00:00:00
category: releases
tags:     []
---
[Mesa 19.1.8](relnotes/19.1.8.html) is released. This is a bug-fix
release.

NOTE: It is anticipated that 19.1.8 will be the final release in the
19.1 series. Users of 19.1 are encouraged to migrate to the 19.2 series
in order to obtain future fixes.
