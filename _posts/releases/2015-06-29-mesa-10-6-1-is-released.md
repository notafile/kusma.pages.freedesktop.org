---
title:    "Mesa 10.6.1 is released"
date:     2015-06-29 00:00:00
category: releases
tags:     []
---
[Mesa 10.6.1](relnotes/10.6.1.html) is released. This is a bug-fix
release.
