---
title:    "Mesa 17.3.9 is released"
date:     2018-04-18 00:00:00
category: releases
tags:     []
---
[Mesa 17.3.9](relnotes/17.3.9.html) is released. This is a bug-fix
release.

NOTE: It is anticipated that 17.3.9 will be the final release in the
17.3 series. Users of 17.3 are encouraged to migrate to the 18.0 series
in order to obtain future fixes.
