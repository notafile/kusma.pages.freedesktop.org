---
title:    "Mesa 6.5.2 has been released"
date:     2006-12-02 00:00:00
category: releases
tags:     []
---
[Mesa 6.5.2](relnotes/6.5.2.html) has been released. This is a new
development release.
