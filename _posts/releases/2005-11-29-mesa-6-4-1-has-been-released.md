---
title:    "Mesa 6.4.1 has been released"
date:     2005-11-29 00:00:00
category: releases
tags:     []
---
[Mesa 6.4.1](relnotes/6.4.1.html) has been released. This is stable,
bug-fix release.
