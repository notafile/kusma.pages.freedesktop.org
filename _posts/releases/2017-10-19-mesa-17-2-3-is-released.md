---
title:    "Mesa 17.2.3 is released"
date:     2017-10-19 00:00:00
category: releases
tags:     []
---
[Mesa 17.2.3](relnotes/17.2.3.html) is released. This is a bug-fix
release.
