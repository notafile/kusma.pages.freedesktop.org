---
title:    "Mesa 10.0.5 is released"
date:     2014-04-18 00:00:00
category: releases
tags:     []
---
[Mesa 10.0.5](relnotes/10.0.5.html) is released. This is a bug-fix
release.

NOTE: Since the 10.1.1 release is being released concurrently, it is
anticipated that 10.0.5 will be the final release in the 10.0 series.
Users of 10.0 are encouraged to migrate to the 10.1 series in order to
obtain future fixes.
