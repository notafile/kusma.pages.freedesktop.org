---
title:    "Mesa 5.0 has been released"
date:     2002-11-13 00:00:00
category: releases
tags:     []
---
Mesa 5.0 has been released. This is a stable release which implements
the OpenGL 1.4 specification.

    New:
        - OpenGL 1.4 support (glGetString(GL_VERSION) returns "1.4")
        - removed some overlooked debugging code
        - glxinfo updated to support GLX_ARB_multisample
        - GLUT now support GLX_ARB_multisample
        - updated DOS driver (Daniel Borca)
    Bug fixes:
        - GL_POINT and GL_LINE-mode polygons didn't obey cull state
        - fixed potential bug in _mesa_align_malloc/calloc()
        - fixed missing triangle bug when running vertex programs
        - fixed a few HPUX compilation problems
        - FX (Glide) driver didn't compile
        - setting GL_TEXTURE_BORDER_COLOR with glTexParameteriv() didn't work
        - a few EXT functions, like glGenTexturesEXT, were no-ops
        - a few OpenGL 1.4 functions like glFogCoord*, glBlendFuncSeparate,
          glMultiDrawArrays and glMultiDrawElements were missing
        - glGet*(GL_ACTIVE_STENCIL_FACE_EXT) was broken
        - Pentium 4 Mobile was mistakenly identified as having 3DNow!
        - fixed one-bit error in point/line fragment Z calculation
        - fixed potential segfault in fakeglx code
        - fixed color overflow problem in DOT3 texture env mode
