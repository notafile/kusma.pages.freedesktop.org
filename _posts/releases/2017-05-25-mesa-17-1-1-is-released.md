---
title:    "Mesa 17.1.1 is released"
date:     2017-05-25 00:00:00
category: releases
tags:     []
---
[Mesa 17.1.1](relnotes/17.1.1.html) is released. This is a bug-fix
release.
