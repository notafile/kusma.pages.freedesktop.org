---
title:    "Mesa 10.5.0 is released"
date:     2015-03-06 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.0](relnotes/10.5.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
