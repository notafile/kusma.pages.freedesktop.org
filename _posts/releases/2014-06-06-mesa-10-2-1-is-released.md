---
title:    "Mesa 10.2.1 is released"
date:     2014-06-06 00:00:00
category: releases
tags:     []
---
[Mesa 10.2.1](relnotes/10.2.1.html) is released. This release only fixes
a build error in the radeonsi driver that was introduced between
10.2-rc5 and the 10.2 final release.
