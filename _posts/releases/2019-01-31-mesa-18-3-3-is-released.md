---
title:    "Mesa 18.3.3 is released"
date:     2019-01-31 00:00:00
category: releases
tags:     []
---
[Mesa 18.3.3](relnotes/18.3.3.html) is released. This is a bug-fix
release.
