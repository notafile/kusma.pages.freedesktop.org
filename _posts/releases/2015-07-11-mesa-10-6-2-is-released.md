---
title:    "Mesa 10.6.2 is released"
date:     2015-07-11 00:00:00
category: releases
tags:     []
---
[Mesa 10.6.2](relnotes/10.6.2.html) is released. This is a bug-fix
release.
