---
title:    "Mesa 9.1.2 is released"
date:     2013-04-30 00:00:00
category: releases
tags:     []
---
[Mesa 9.1.2](relnotes/9.1.2.html) is released. This is a bug fix
release.
