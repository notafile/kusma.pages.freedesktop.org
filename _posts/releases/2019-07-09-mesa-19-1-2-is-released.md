---
title:    "Mesa 19.1.2 is released"
date:     2019-07-09 00:00:00
category: releases
tags:     []
---
[Mesa 19.1.2](relnotes/19.1.2.html) is released. This is a bug-fix
release.
