---
title:    "Mesa 10.5.4 is released"
date:     2015-04-24 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.4](relnotes/10.5.4.html) is released. This is a bug-fix
release.
