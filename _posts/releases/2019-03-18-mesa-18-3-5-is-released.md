---
title:    "Mesa 18.3.5 is released"
date:     2019-03-18 00:00:00
category: releases
tags:     []
---
[Mesa 18.3.5](relnotes/18.3.5.html) is released. This is a bug-fix
release.
