---
title:    "Mesa 17.3.6 is released"
date:     2018-02-26 00:00:00
category: releases
tags:     []
---
[Mesa 17.3.6](relnotes/17.3.6.html) is released. This is a bug-fix
release.
