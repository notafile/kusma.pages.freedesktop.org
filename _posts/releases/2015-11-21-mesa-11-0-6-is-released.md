---
title:    "Mesa 11.0.6 is released"
date:     2015-11-21 00:00:00
category: releases
tags:     []
---
[Mesa 11.0.6](relnotes/11.0.6.html) is released. This is a bug-fix
release.
