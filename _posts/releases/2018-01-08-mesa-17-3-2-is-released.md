---
title:    "Mesa 17.3.2 is released"
date:     2018-01-08 00:00:00
category: releases
tags:     []
---
[Mesa 17.3.2](relnotes/17.3.2.html) is released. This is a bug-fix
release.
