---
title:    "Mesa 4.0.2 has been released"
date:     2002-04-02 00:00:00
category: releases
tags:     []
---
Mesa 4.0.2 has been released. This is a stable bug-fix release.

``` 
    New:
      - New DOS (DJGPP) driver written by Daniel Borca
      - New driver interface functions for TCL drivers (such as Radeon DRI)
      - GL_RENDERER string returns "Mesa Offscreen16" or "Mesa Offscreen32"
        if using deep color channels
      - latest GL/glext.h and GL/glxext.h headers from SGI
    Bug fixes:
      - GL_BLEND with non-black texture env color wasn't always correct
      - GL_REPLACE with GL_RGB texture format wasn't always correct (alpha)
      - glTexEnviv( pname != GL_TEXTURE_ENV_COLOR ) was broken
      - glReadPixels was sometimes mistakenly clipped by the scissor box
      - glDraw/ReadPixels didn't catch all the errors that they should have
      - Fixed 24bpp rendering problem in Windows driver (Karl Schultz)
      - 16-bit GLchan mode fixes (m_trans_tmp.h, s_triangle.c)
      - Fixed 1-bit float->int conversion bug in glDrawPixels(GL_DEPTH_COMP)
      - glColorMask as sometimes effecting glXSwapBuffers()
      - fixed a potential bug in XMesaGarbageCollect()
      - N threads rendering into one window didn't work reliably
      - glCopyPixels didn't work for deep color channels
      - improved 8 -> 16bit/channel texture image conversion (Gerk Huisma)
      - glPopAttrib() didn't correctly restore user clip planes
      - user clip planes failed for some perspective projections (Chromium)
```
