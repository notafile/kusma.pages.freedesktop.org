---
title:    "Mesa 3.4 has been released"
date:     2000-11-03 00:00:00
category: releases
tags:     []
---
Mesa 3.4 has been released. Here's what's new since the 3.3 release:

``` 
    New:
    - optimized glDrawPixels for glPixelZoom(1,-1)
    Bug Fixes:
    - widgets-mesa/src/*.c files were missing from 3.3 distro
    - include/GL/mesa_wgl.h file was missing from 3.3 distro
    - fixed some Win32 compile problems
    - texture object priorities weren't getting initialized to 1.0
    - glAreTexturesResident return value was wrong when using hardware
    - glXUseXFont segfaulted when using 3dfx driver (via MESA_GLX_FX)
    - glReadPixels with GLushort packed types was broken
    - fixed a few bugs in the GL_EXT_texture_env_combine texture code
    - glPush/PopAttrib(GL_ENABLE_BIT) mishandled multi-texture enables
    - fixed some typos/bugs in the VB code
    - glDrawPixels(GL_COLOR_INDEX) to RGB window didn't work
    - optimized glDrawPixels paths weren't being used
    - per-fragment fog calculation didn't work without a Z buffer
    - improved blending accuracy, fixes Glean  blendFunc test failures
    - glPixelStore(GL_PACK/UNPACK_SKIP_IMAGES) wasn't handled correctly
    - glXGetProcAddressARB() didn't always return the right address
    - gluBuild[12]DMipmaps() didn't grok the GL_BGR pixel format
    - texture matrix changes weren't always detected (GLUT projtex demo)
    - fixed random color problem in vertex fog code
    - fixed Glide-related bug that let Quake get a 24-bit Z buffer
    Changes:
    - finished internal support for compressed textures for DRI
```
