---
title:    "Mesa 13.0.5 is released"
date:     2017-02-20 00:00:00
category: releases
tags:     []
---
[Mesa 13.0.5](relnotes/13.0.5.html) is released. This is a bug-fix
release.
