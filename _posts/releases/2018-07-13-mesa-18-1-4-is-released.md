---
title:    "Mesa 18.1.4 is released"
date:     2018-07-13 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.4](relnotes/18.1.4.html) is released. This is a bug-fix
release.
