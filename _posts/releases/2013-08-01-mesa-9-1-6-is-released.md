---
title:    "Mesa 9.1.6 is released"
date:     2013-08-01 00:00:00
category: releases
tags:     []
---
[Mesa 9.1.6](relnotes/9.1.6.html) is released. This is a bug fix
release.
