---
title:    "Mesa 7.7.1 is released"
date:     2010-03-28 00:00:00
category: releases
tags:     []
---
[Mesa 7.7.1](relnotes/7.7.1.html) is released. This is a bug-fix release
fixing issues found in the 7.7 release.

Also, [Mesa 7.8](relnotes/7.8.html) is released. This is a new
development release.
