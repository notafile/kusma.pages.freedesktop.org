---
title:    "Mesa 9.0.1 is released"
date:     2012-11-16 00:00:00
category: releases
tags:     []
---
[Mesa 9.0.1](relnotes/9.0.1.html) is released. This is a bug fix
release.
