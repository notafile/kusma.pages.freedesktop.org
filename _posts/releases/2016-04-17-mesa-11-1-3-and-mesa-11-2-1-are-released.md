---
title:    "Mesa 11.1.3 and Mesa 11.2.1 are released"
date:     2016-04-17 00:00:00
category: releases
tags:     []
---
[Mesa 11.1.3](relnotes/11.1.3.html) and [Mesa
11.2.1](relnotes/11.2.1.html) are released. These are bug-fix releases
from the 11.1 and 11.2 branches, respectively.
