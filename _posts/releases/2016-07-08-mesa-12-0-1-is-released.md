---
title:    "Mesa 12.0.1 is released"
date:     2016-07-08 00:00:00
category: releases
tags:     []
---
[Mesa 12.0.1](relnotes/12.0.1.html) is released. This is a bug-fix
release, resolving build issues in the r600 and radeonsi drivers.

[Mesa 12.0.0](relnotes/12.0.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
