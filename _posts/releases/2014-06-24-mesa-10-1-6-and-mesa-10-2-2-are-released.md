---
title:    "Mesa 10.1.6 and Mesa 10.2.2 are released"
date:     2014-06-24 00:00:00
category: releases
tags:     []
---
[Mesa 10.1.6](relnotes/10.1.6.html) and [Mesa
10.2.2](relnotes/10.2.2.html) are released. These are bug-fix releases
from the 10.1 and 10.2 branches, respectively.
