---
title:    "Mesa 10.6.8 is released"
date:     2015-09-20 00:00:00
category: releases
tags:     []
---
[Mesa 10.6.8](relnotes/10.6.8.html) is released. This is a bug-fix
release.
