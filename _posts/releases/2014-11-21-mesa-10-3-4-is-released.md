---
title:    "Mesa 10.3.4 is released"
date:     2014-11-21 00:00:00
category: releases
tags:     []
---
[Mesa 10.3.4](relnotes/10.3.4.html) is released. This is a bug-fix
release.
