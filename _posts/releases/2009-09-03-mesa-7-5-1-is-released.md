---
title:    "Mesa 7.5.1 is released"
date:     2009-09-03 00:00:00
category: releases
tags:     []
---
[Mesa 7.5.1](relnotes/7.5.1.html) is released. This is a bug-fix release
which fixes bugs found in version 7.5.
