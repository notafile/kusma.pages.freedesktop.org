---
title:    "Mesa 7.11.1 is released"
date:     2011-11-17 00:00:00
category: releases
tags:     []
---
[Mesa 7.11.1](relnotes/7.11.1.html) is released. This is a bug fix
release.
