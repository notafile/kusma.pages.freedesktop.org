---
title:    "Mesa 17.1.6 is released"
date:     2017-08-07 00:00:00
category: releases
tags:     []
---
[Mesa 17.1.6](relnotes/17.1.6.html) is released. This is a bug-fix
release.
