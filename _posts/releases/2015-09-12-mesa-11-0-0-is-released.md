---
title:    "Mesa 11.0.0 is released"
date:     2015-09-12 00:00:00
category: releases
tags:     []
---
[Mesa 11.0.0](relnotes/11.0.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
