---
title:    "Mesa 18.2.4 is released"
date:     2018-10-31 00:00:00
category: releases
tags:     []
---
[Mesa 18.2.4](relnotes/18.2.4.html) is released. This is a bug-fix
release.
