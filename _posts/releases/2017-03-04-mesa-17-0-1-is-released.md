---
title:    "Mesa 17.0.1 is released"
date:     2017-03-04 00:00:00
category: releases
tags:     []
---
[Mesa 17.0.1](relnotes/17.0.1.html) is released. This is a bug-fix
release.
