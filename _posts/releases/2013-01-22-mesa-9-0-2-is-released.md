---
title:    "Mesa 9.0.2 is released"
date:     2013-01-22 00:00:00
category: releases
tags:     []
---
[Mesa 9.0.2](relnotes/9.0.2.html) is released. This is a bug fix
release.
