---
title:    "Mesa 4.1 has been released"
date:     2002-10-29 00:00:00
category: releases
tags:     []
---
Mesa 4.1 has been released. This is a new development release. For a
stable release, get 4.0.4.

    New:
        - GL_NV_vertex_program extension
        - GL_NV_vertex_program1_1 extension
        - GL_ARB_window_pos extension
        - GL_ARB_depth_texture extension
        - GL_ARB_shadow extension
        - GL_ARB_shadow_ambient extension
        - GL_EXT_shadow_funcs extension
        - GL_ARB_point_parameters extension
        - GL_ARB_texture_env_crossbar
        - GL_NV_point_sprite extension
        - GL_NV_texture_rectangle extension
        - GL_EXT_multi_draw_arrays extension
        - GL_EXT_stencil_two_side extension
        - GLX_SGIX_fbconfig and GLX_SGIX_pbuffer extensions
        - GL_ATI_texture_mirror_once extension (Ian Romanick)
        - massive overhaul/simplification of software rasterizer module,
          many contributions from Klaus Niederkrueger
        - faster software texturing in some cases (i.e. trilinear filtering)
        - new OSMesaGetProcAddress() function
        - more blend modes implemented with MMX code (Jose Fonseca)
        - added glutGetProcAddress() to GLUT
        - added GLUT_FPS env var to compute frames/second in glutSwapBuffers()
        - pbinfo and pbdemo PBuffer programs
        - glxinfo -v prints transprent pixel info (Gerd Sussner)
    Bug fixes:
        - better mipmap LOD computation (prevents excessive blurriness)
        - OSMesaMakeCurrent() didn't recognize buffer size changes
        - assorted conformance fixes for 16-bit/channel rendering
        - texcombine alpha subtraction mode was broken
        - fixed some blend problems when GLchan==GLfloat (Gerk Huisma)
        - clamp colors to [0,1] in OSMesa if GLchan==GLfloat (Gerk Huisma)
        - fixed divide by zero error in NURBS tessellator (Jon Perry)
        - fixed GL_LINEAR fog bug by adding clamping
        - fixed FP exceptions found using Alpha CPU
        - 3dfx/glide driver render-to-window feature was broken
        - added missing GLX_TRANSPARENT_RGB token to glx.h
        - fixed error checking related to paletted textures
        - fixed reference count error in glDeleteTextures (Randy Fayan)
    Changes:
        - New spec file and Python code to generate some GL dispatch files
        - Glide driver defaults to "no" with autoconf/automake
        - floating point color channels now clamped to [0,inf)
        - updated demos/stex3d with new options
