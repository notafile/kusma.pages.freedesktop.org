---
title:    "Mesa 10.5.7 is released"
date:     2015-06-07 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.7](relnotes/10.5.7.html) is released. This is a bug-fix
release.
