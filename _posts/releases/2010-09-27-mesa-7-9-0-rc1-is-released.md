---
title:    "Mesa 7.9.0-rc1 is released"
date:     2010-09-27 00:00:00
category: releases
tags:     []
---
[Mesa 7.9.0-rc1](relnotes/7.9.html) is released. This is a release
candidate for the 7.9 development release.
