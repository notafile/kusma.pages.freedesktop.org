---
title:    "Mesa 9.0.3 is released"
date:     2013-02-21 00:00:00
category: releases
tags:     []
---
[Mesa 9.0.3](relnotes/9.0.3.html) is released. This is a bug fix
release.
