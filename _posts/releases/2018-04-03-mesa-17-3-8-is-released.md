---
title:    "Mesa 17.3.8 is released"
date:     2018-04-03 00:00:00
category: releases
tags:     []
---
[Mesa 17.3.8](relnotes/17.3.8.html) is released. This is a bug-fix
release.
