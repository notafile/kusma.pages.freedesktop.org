---
title:    "Mesa 18.2.2 is released"
date:     2018-10-05 00:00:00
category: releases
tags:     []
---
[Mesa 18.2.2](relnotes/18.2.2.html) is released. This is a bug-fix
release.
