---
title:    "Mesa 7.0.1 is released"
date:     2007-08-03 00:00:00
category: releases
tags:     []
---
[Mesa 7.0.1](relnotes/7.0.1.html) is released. This is a bug-fix
release.
