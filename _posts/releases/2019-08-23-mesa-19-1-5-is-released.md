---
title:    "Mesa 19.1.5 is released"
date:     2019-08-23 00:00:00
category: releases
tags:     []
---
[Mesa 19.1.5](relnotes/19.1.5.html) is released. This is a bug-fix
release.
