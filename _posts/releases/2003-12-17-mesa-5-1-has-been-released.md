---
title:    "Mesa 5.1 has been released"
date:     2003-12-17 00:00:00
category: releases
tags:     []
---
Mesa 5.1 has been released. This is a new development release. Mesa 6.0
will be the next stable release and will support all OpenGL 1.5
features.

``` 
    New features:
    - reorganized directory tree
    - GL_ARB_vertex/fragment_program extensions (Michal Krol & Karl Rasche)
    - GL_ATI_texture_env_combine3 extension (Ian Romanick)
    - GL_SGI_texture_color_table extension (Eric Plante)
    - GL_NV_fragment_program extension
    - GL_NV_light_max_exponent extension
    - GL_EXT_texture_rectangle (identical to GL_NV_texture_rectangle)
    - GL_ARB_occlusion_query extension
    - GL_ARB_point_sprite extension
    - GL_ARB_texture_non_power_of_two extension
    - GL_IBM_multimode_draw_arrays extension
    - GL_EXT_texture_mirror_clamp extension (Ian Romanick)
    - GL_ARB_vertex_buffer_object extension
    - new X86 feature detection code (Petr Sebor)
    - less memory used for display lists and vertex buffers
    - demo of per-pixel lighting with a fragment program (demos/fplight.c)
    - new version (18) of glext.h header
    - new spriteblast.c demo of GL_ARB_point_sprite
    - faster glDrawPixels in X11 driver in some cases (see relnotes/5.1)
    - faster glCopyPixels in X11 driver in some cases (see relnotes/5.1)
    Bug fixes:
    - really enable OpenGL 1.4 features in DOS driver.
    - fixed issues in glDrawPixels and glCopyPixels for very wide images
    - glPixelMapf/ui/usv()'s size parameter is GLsizei, not GLint
    - fixed some texgen bugs reported by Daniel Borca
    - fixed wglMakeCurrent(NULL, NULL) bug (#835861)
    - fixed glTexSubImage3D z-offset bug (Cedric Gautier)
    - fixed RGBA blend enable bug (Ville Syrjala)
    - glAccum is supposed to be a no-op in selection/feedback mode
    - fixed texgen bug #597589 (John Popplewell)
    Changes:
    - dropped API trace feature (src/Trace/)
    - documentation overhaul.  merged with website content.  more html.
    - glxgears.c demo updated to use GLX swap rate extensions
    - glTexImage1/2/3D now allows width/height/depth = 0
    - disable SPARC asm code on Linux (bug 852204)
```

The MD5 checksums are:

    78f452f6c55478471a744f07147612b5  MesaLib-5.1.tar.gz
    67b3b8d3f7f4c8c44904551b851d01af  MesaLib-5.1.tar.bz2
    6dd19ffa750ec7f634e370a987505c9d  MesaLib-5.1.zip
    e0214d4ebb22409dfa9262f2b52fd828  MesaDemos-5.1.tar.gz
    066c9aff4fd924405de1ae9bad5ec9a7  MesaDemos-5.1.tar.bz2
    d2b5ba32b53e0ad0576c637a4cc1fb41  MesaDemos-5.1.zip
