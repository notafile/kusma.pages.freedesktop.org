---
title:    "Mesa 17.1.9 is released"
date:     2017-09-08 00:00:00
category: releases
tags:     []
---
[Mesa 17.1.9](relnotes/17.1.9.html) is released. This is a bug-fix
release.
