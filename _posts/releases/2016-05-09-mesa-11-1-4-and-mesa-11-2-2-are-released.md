---
title:    "Mesa 11.1.4 and Mesa 11.2.2 are released"
date:     2016-05-09 00:00:00
category: releases
tags:     []
---
[Mesa 11.1.4](relnotes/11.1.4.html) and [Mesa
11.2.2](relnotes/11.2.2.html) are released. These are bug-fix releases
from the 11.1 and 11.2 branches, respectively.

NOTE: It is anticipated that 11.1.4 will be the final release in the
11.1.4 series. Users of 11.1 are encouraged to migrate to the 11.2
series in order to obtain future fixes.
