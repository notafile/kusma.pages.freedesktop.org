---
title:    "Mesa 19.1.4 is released"
date:     2019-08-07 00:00:00
category: releases
tags:     []
---
[Mesa 19.1.4](relnotes/19.1.4.html) is released. This is a bug-fix
release.
