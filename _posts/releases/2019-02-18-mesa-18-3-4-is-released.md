---
title:    "Mesa 18.3.4 is released"
date:     2019-02-18 00:00:00
category: releases
tags:     []
---
[Mesa 18.3.4](relnotes/18.3.4.html) is released. This is a bug-fix
release.
