---
title:    "Mesa 17.3.7 is released"
date:     2018-03-21 00:00:00
category: releases
tags:     []
---
[Mesa 17.3.7](relnotes/17.3.7.html) is released. This is a bug-fix
release.
