---
title:    "Mesa 17.1.5 is released"
date:     2017-07-14 00:00:00
category: releases
tags:     []
---
[Mesa 17.1.5](relnotes/17.1.5.html) is released. This is a bug-fix
release.
